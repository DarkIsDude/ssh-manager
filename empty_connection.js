module.exports = () => {
    return {
        id: undefined,
        name: 'name',
        host: '',
        username: '',
        password: '',
        icon: 'user',
        parent: null,

        public_key: false,
        pem: '',
    }
}
