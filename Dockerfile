FROM node:8

RUN apt-get update

# Install yarn
RUN apt-get -y install apt-transport-https
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get -y install yarn

# Install package
RUN apt-get update
RUN apt-get update && apt-get -y autoremove && apt-get clean

# Prepare build process
RUN mkdir /app
COPY . /app/
WORKDIR /app

# Build process
RUN yarn install
RUN yarn build
