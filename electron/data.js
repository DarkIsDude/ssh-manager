const fs = require('fs')
const path = require('path')
const { app } = require('electron')

const PATH_DATA = path.join(app.getPath('userData'), 'data.json')

console.info('data from', PATH_DATA)

function read () {
    if (fs.existsSync(PATH_DATA)) {
        return JSON.parse(fs.readFileSync(PATH_DATA, 'utf8')) || []
    } else {
        return { window: { size: undefined, position: undefined }, connections: [] }
    }
}

function updateConnections (connections) {
    const data = read()
    data.connections = connections || []
    fs.writeFileSync(PATH_DATA, JSON.stringify(data), 'utf8')
}

function updateWindow (size, position) {
    const data = read()
    data.window = { size, position }
    fs.writeFileSync(PATH_DATA, JSON.stringify(data), 'utf8')
}

module.exports = {
    getConnections: () => read().connections,
    updateConnections,

    getWindow: () => read().window,
    updateWindow,
}
