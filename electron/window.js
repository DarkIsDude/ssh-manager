const { app, BrowserWindow } = require('electron')
const data = require('./data')
const path = require('path')

let mainWindow
function createWindow () {
    if (!mainWindow) {
        const icon = path.join(__dirname, '..', 'public', 'logo.png')
        app.dock.setIcon(icon)

        mainWindow = new BrowserWindow()

        const windowInit = data.getWindow()

        if (windowInit.position) {
            mainWindow.setPosition(windowInit.position[0], windowInit.position[1])
        }

        if (windowInit.size) {
            mainWindow.setSize(windowInit.size[0], windowInit.size[1])
        }

        mainWindow.loadURL(`file://${__dirname}/vue/index.html`)

        // Open the DevTools.
        // mainWindow.webContents.openDevTools()

        mainWindow.on('closed', () => {
            mainWindow = null
        })

        mainWindow.on('resize', (event) => {
            data.updateWindow(event.sender.getSize(), event.sender.getPosition())
        })

        mainWindow.on('move', (event) => {
            data.updateWindow(event.sender.getSize(), event.sender.getPosition())
        })
    }
}

const OPEN_EDITOR_WINDOW_SIZE = 1200
const HIDE_EDITOR_WINDOW_SIZE = 300
let editorOpened = false
function toggleEditor (fromUI = false) {
    const size = mainWindow.getSize()
    size[0] = editorOpened ? HIDE_EDITOR_WINDOW_SIZE : OPEN_EDITOR_WINDOW_SIZE
    editorOpened = !editorOpened

    mainWindow.setSize(size[0], size[1])
    data.updateWindow(mainWindow.getSize(), mainWindow.getPosition())

    if (!fromUI) {
        mainWindow.webContents.send('toggle-editor')
    }
}

module.exports = {
    createWindow,
    toggleEditor,
}
