const { ipcMain, app } = require('electron')
const { exec } = require('child_process')
const path = require('path')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const window = require('./window')
const data = require('./data')
const empty = require('../empty_connection')

const FILEZILLA = '/Applications/FileZilla.app/Contents/MacOS/filezilla'
const ITERM = '/Applications/iTerm.app'

ipcMain.on('find', (event) => {
    const connections = data.getConnections()

    if (connections && connections.length > 0) {
        event.returnValue = connections
    } else {
        const value = empty()
        value.id = uuidv4()
        event.returnValue = [value]
    }
})

ipcMain.on('update', (event, connection) => {
    data.updateConnections(connection)
    event.returnValue = data.getConnections()
})

ipcMain.on('ssh', (event, connection) => {
    let content

    if (connection.public_key) {
        content = `#!/bin/sh -f
ssh ${connection.username}@${connection.host}`
    } else if (connection.pem) {
        content = `#!/bin/sh -f
ssh -i ${connection.pem} ${connection.username}@${connection.host}`
    } else {
        content = `#!/usr/bin/expect -f
spawn ssh ${connection.username}@${connection.host}
match_max 100000
expect "*?assword:*"
send -- "${connection.password}\r"
interact`
    }

    const filename = Math.floor(Math.random() * 1000) + '.sh'
    const filepath = path.join(app.getPath('temp'), filename)
    fs.writeFileSync(filepath, content, 'utf8')
    fs.chmodSync(filepath, 0o700)
    console.info('Write into', filepath)

    const cmd = `/usr/bin/open -a ${ITERM} ${filepath}`
    exec(cmd, (err) => {
        if (err) {
            console.error(err)
        }
    })
})

ipcMain.on('sftp', (event, connection) => {
    const password = connection.password ? ':' + connection.password : ''
    const cmd = `${FILEZILLA} sftp://${connection.username}${password}@${connection.host}`

    exec(cmd, (err, stdout, stderr) => {
        if (err) {
            console.error(err)
        }
    })
})

ipcMain.on('toggle-editor', () => window.toggleEditor(true))
