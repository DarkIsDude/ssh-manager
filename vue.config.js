const path = require('path')

module.exports = {
    baseUrl: './',
    configureWebpack: {
        output: {
            publicPath: './',
            path: path.resolve(__dirname, './electron/vue'),
        },
    },
}
