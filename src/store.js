import Vue from 'vue'
import Vuex from 'vuex'
import uuidv4 from 'uuid/v4'
import empty from '../empty_connection'

Vue.use(Vuex)

function findDeep (items, value) {
    if (items) {
        let i = 0
        while (i < items.length) {
            const children = findDeep(items[i].children, value)

            if (items[i].id === value.id) {
                return items[i]
            } else if (children) {
                return children
            }

            i++
        }
    }
}

function findParentDeep (items, value) {
    if (items) {
        let i = 0
        while (i < items.length) {
            if (items[i].id === value.id) {
                return true
            }

            const children = findParentDeep(items[i].children, value)
            if (children === true) {
                return items[i]
            } else if (children) {
                return children
            }

            i++
        }
    }
}

function updateDeep (items, value) {
    if (items) {
        let i = 0
        while (i < items.length) {
            if (items[i].id === value.id) {
                items[i] = value
                return items
            }

            if (updateDeep(items[i].children, value)) {
                return items
            }

            i++
        }
    }
}

function removeDeep (items, value) {
    if (items) {
        let i = 0
        while (i < items.length) {
            if (items[i].id === value.id) {
                items.splice(i, 1)
                return items
            }

            if (removeDeep(items[i].children, value)) {
                return items
            }

            i++
        }
    }
}

function flattenDeep (items) {
    if (items) {
        const flat = []

        items.forEach(item => {
            flat.push(item)

            flattenDeep(item.children).reduce((flat, child) => {
                flat.push(child)
                return flat
            }, flat)
        })

        return flat
    } else {
        return []
    }
}

export default new Vuex.Store({
    state: {
        connections: [],
        selected: null,
    },
    mutations: {
        reset (state, connections) {
            state.connections = connections
        },
        update (state, connection) {
            state.connections = updateDeep(state.connections, connection)
        },
        select (state, connection) {
            state.selected = findDeep(state.connections, connection)
        },
        remove (state, connection) {
            state.connections = removeDeep(state.connections, connection)
        },
        add (state, connection) {
            const find = findDeep(state.connections, connection)

            if (!find.children) {
                find.children = []
            }

            const value = empty()
            value.id = uuidv4()
            find.children.push(value)
        },
        addRoot (state, connection) {
            state.connections.push(connection)
        },
    },
    getters: {
        connections: state => state.connections,
        connectionsFlatten: state => flattenDeep(state.connections),

        connectionFind: state => connection => findDeep(state.connections, connection),
        connectionParent: state => connection => findParentDeep(state.connections, connection),

        selected: state => state.selected,
    },
    actions: {

    },
})
