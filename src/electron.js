module.exports = {
    find: () => window.electron.ipcRenderer.sendSync('find'),
    update: (connections) => window.electron.ipcRenderer.sendSync('update', connections),
    ssh: (connection) => window.electron.ipcRenderer.send('ssh', connection),
    sftp: (connection) => window.electron.ipcRenderer.send('sftp', connection),
    toggleEditor: () => window.electron.ipcRenderer.send('toggle-editor'),

    listen: (channel, listener) => window.electron.ipcRenderer.on(channel, listener),
}
